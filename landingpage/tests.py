from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from .models import Status
from .forms import FormStatus
from .views import landing_page
from django.http import HttpRequest
import time
# # Create your tests here.
class story6FunctionalTests(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story6FunctionalTests, self).setUp()

    def test_input_todo(self):
        selenium = self.selenium
        option = Options()           
        option.headless = True
        selenium.get('http://127.0.0.1:8000/')
          
        form = selenium.find_element_by_name('status')
        time.sleep(5)
        form.send_keys('Coba coba nih')
        submit = selenium.find_element_by_id('button_submit')       
        
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        
        self.assertIn('Coba coba nih', selenium.page_source)
        self.assertNotIn('ini cuma test status yg harusnya gaada', selenium.page_source)

    
    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTests, self).tearDown()


class story6UnitTests(TestCase):
    def test_landingpage_url_is_resolved(self):
       r = Client().get('/')
       self.assertEqual(r.status_code, 200)

    def test_landingpage_template_are_correct(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_static_files(self):
        self.assertContains(Client().get('/'), 'style.css')
    
    def test_hello_apa_kabar_exist(self):
        request = HttpRequest()
        response = landing_page(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, Apa Kabar?', html_response)
        
    def test_using_about_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing_page)
    
    def setUp(self):
        Status.objects.create(status='lagi capek')
    
    def test_model_status(self):
        test_object = Status.objects.get(status='lagi capek')
        self.assertEquals(test_object.status, 'lagi capek')

    def test_model_time(self):
        test_object = Status.objects.get(status='lagi capek')
        self.assertTrue(test_object.time)

    def test_model_max_length(self):
        test_object = Status.objects.get(status='lagi capek')
        max_length = test_object._meta.get_field('status').max_length
        self.assertEquals(max_length, 300)

    def test_forms_valid(self):
        form_data = {'status': 'ngetest valid nih cuy'}
        form = FormStatus(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        form_data = {'status': 'Django membantu pengembang menghindari banyak kesalahan keamanan yang umum dengan menyediakan kerangka kerja yang telah direkayasa untuk "melakukan hal yang benar" untuk melindungi situs secara otomatis. Sebagai contoh, Django menyediakan cara aman untuk mengelola akun pengguna dan kata sandi, menghindari kesalahan umum seperti memasukkan informasi sesi ke dalam cookie dimana file tersebut rentan (bahkan cookies hanya berisi sebuah kunci, dan data sebenarnya disimpan dalam database) atau langsung menyimpan password bukan hash password.'}
        form = FormStatus(data=form_data)
        self.assertFalse(form.is_valid())

    def test_save_POST_request(self):

        response = self.client.post('/', data={'status' : 'TDD oh TDD'})
        
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('TDD oh TDD', html_response)

    