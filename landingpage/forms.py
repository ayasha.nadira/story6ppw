from django.db import models
from django import forms
from . import models

class FormStatus(forms.ModelForm):
    status = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Write Your Status", "class" : "form_status"}))
    # status = forms.CharField(widget=forms.Textarea(attrs = {"placeholder" : "Write Your Status", "class" : "formstyle"}))               
    class Meta:
        model = models.Status
        fields = [
                'status',
                ]