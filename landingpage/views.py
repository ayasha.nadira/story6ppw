from django.http import HttpResponse
from django.shortcuts import render
from .forms import FormStatus
from .models import Status

def landing_page(request):
    allStatus = Status.objects.all()
    if request.method == "POST":
        form = FormStatus(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'index.html', {'form' : form, 'status' : allStatus})
    else:
        form = FormStatus()
        return render(request, 'index.html', {'form' : form, 'status' : allStatus})
