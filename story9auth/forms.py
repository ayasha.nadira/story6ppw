from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
class Formlogin(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Username", "class" : "form-control"}))
    password = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Password", "class" : "form-control", "type" : "password"}))

class Formsignup (UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Username", "class" : "form-control"}))
    password1 = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Password (Minimum 8 characters)", "class" : "form-control", "type" : "password"}))
    password2 = forms.CharField(widget=forms.TextInput(attrs = {"placeholder" : "Confirm Password", "class" : "form-control", "type" : "password"}))
    
    
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']