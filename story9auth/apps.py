from django.apps import AppConfig


class Story9AuthConfig(AppConfig):
    name = 'story9auth'
