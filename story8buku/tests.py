from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from .views import story8page
from django.http import HttpRequest
import time
# Create your tests here.
class story8UnitTests(TestCase):
    def test_story8_url_is_resolved(self):
       r = Client().get('/story8/')
       self.assertEqual(r.status_code, 200)

    def test_landingpage_template_are_correct(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')
        
    def test_static_files(self):
        self.assertContains(Client().get('/story8/'), 'story8_style.css')
        self.assertContains(Client().get('/story8/'), 'searchbooks.js')

    def test_using_about_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8page)
        
    def test_heading_button_books_exist(self):
        request = HttpRequest()
        response = story8page(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Search your Books!', html_response)
        self.assertIn('Search', html_response)

        
        



